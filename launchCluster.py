#!/usr/bin/env python

import boto3, aws, argparse, os, polling, sys, requests, datetime, time, re, zipfile
from subprocess import call

SPARK_PATHS = {
    '1.6.0': '/home/ubuntu/spark-1.6.0-bin-hadoop',
    '2.0.0': '/home/ubuntu/spark-2.0.0-bin-hadoop'
}

if __name__ == '__main__':
    p = argparse.ArgumentParser (description = 'Launches a SPARK cluster.')
    p.add_argument ('--jar', type=str, required=True, help='The (fat) JAR for the job')
    p.add_argument ('--main', type=str, required=True, help='The main class from the JAR to use for the job')
    p.add_argument ('--key', type=str, required=True, help='The AWS keypair to use for instances')
    p.add_argument ('--size', type=int, required=True, help='The number of EC2 instances (except the driver) in the cluster')
    p.add_argument ('--driver_type', type=str, required=True, help='The EC2 instance type for the driver')
    p.add_argument ('--worker_type', type=str, required=True, help='The EC2 instance type for the workers')
    p.add_argument ('--executor_count', type=int, required=False, default=1, help='The number of executors per worker (Default: 1)')
    p.add_argument ('--spark_version', type=str, required=False, default='1.6.0', help='The Spark version to use (Default: 1.6.0)')
    p.add_argument ('--hadoop_version', type=str, required=False, default='2.4', help='The Spark version to use (Default: 2.4)')
    p.add_argument ('--input', type=str, required=True, help='S3 (native) URI for input')
    p.add_argument ('--output', type=str, required=True, help='S3 (native) URI for output')
    p.add_argument ('--dir', type=str, required=False, help='Path to a directory to be copied to the driver')
    p.add_argument ('--conf', type=str, required=False, help='Conf file to pass to driver (e.g. log4j.properties)')
    args = p.parse_args ()


def get_spark_path():
    return SPARK_PATHS[args.spark_version] + args.hadoop_version

# Crude check on jarfile

if not zipfile.is_zipfile(args.jar):
    sys.stderr.write("'" + args.jar + "' does not look like a valid JAR file\n")
    sys.exit(2)

# Sanity check on workers

if args.size < 1:
    sys.stderr.write('A cluster needs some workers! (--size must be 1 or greater)\n')
    sys.exit(2)

# Check instance type for driver

if args.driver_type in aws.instance_resources:
    driver_memory = int(aws.instance_resources[args.driver_type][0] - 2.0)
else:
    sys.stderr.write("'" + args.driver_type + "' is not a known EC2 instance type\n")
    sys.exit(2)

# Check instance type for workers

if args.worker_type in aws.instance_resources:
    (memory, cores) = aws.instance_resources[args.worker_type]
    executor_memory = str(int(((memory - 1.0) / args.executor_count) * 1024)) + 'm'
else:
    sys.stderr.write("'" + args.worker_type + "' is not a known EC2 instance type\n")
    sys.exit(2)

# Pick up the environment variables

if not 'AWS_ACCESS_KEY_ID' in os.environ:
    sys.stderr.write('AWS_ACCESS_KEY_ID must be set in environment\n')
    sys.exit(2)

if not 'AWS_SECRET_ACCESS_KEY' in os.environ:
    sys.stderr.write('AWS_SECRET_ACCESS_KEY must be set in environment\n')
    sys.exit(2)


key_name = os.path.splitext(os.path.basename(args.key))[0]

memory = """
echo '#!/usr/bin/env bash' > """ + get_spark_path() + """/conf/spark-env.sh
echo SPARK_WORKER_MEMORY=""" + executor_memory + """ >> """ + get_spark_path() + """/conf/spark-env.sh
echo SPARK_WORKER_CORES=1 >> """ + get_spark_path() + """/conf/spark-env.sh
#"""

ec2 = boto3.resource('ec2')

# Launch the master first

master_script = """#cloud-boothook
#!/bin/bash
exec > >(tee /var/log/user-data.log|logger -t user-data -s 2>/dev/console) 2>&1
echo export AWS_ACCESS_KEY_ID=""" + os.environ['AWS_ACCESS_KEY_ID'] + """ >> """ + get_spark_path() + """/sbin/spark-config.sh
echo export AWS_SECRET_ACCESS_KEY=""" + os.environ['AWS_SECRET_ACCESS_KEY'] + """ >> """ + get_spark_path() + """/sbin/spark-config.sh
""" + memory + """
sudo -H -u ubuntu """ + get_spark_path() + """/sbin/start-master.sh
"""

def log_time():
    return datetime.datetime.now().strftime("%d/%m/%y %H:%M:%S") + ' INFO: '

print(log_time() + 'Starting master...')

basename = '%s' + os.path.splitext(os.path.basename(args.jar))[0] + '-' + datetime.datetime.now().strftime("%y%m%d%H%M")

master = ec2.create_instances(ImageId=aws.SCUD_AMI, InstanceType=args.driver_type,
                              KeyName=key_name,
                              SubnetId=aws.SCUD_SUBNET,
                              SecurityGroupIds=[aws.SCUD_SG],
                              UserData=master_script,
                              MinCount=1, MaxCount=1)[0]
time.sleep(5)
master.wait_until_running()
ec2.create_tags(Resources=[master.id], Tags=[{'Key': 'Name', 'Value': basename % 'SCUDMaster_'}])


# Wait for the master's Web interface to show

sys.stdout.write(log_time() + 'Polling master (' + master.private_ip_address + ')')
sys.stdout.flush()

def poll_master_alive():
    try:
        return requests.get('http://' + master.private_ip_address + ':8080', timeout=5).status_code == 200
    except:
        sys.stdout.write('.')
        sys.stdout.flush()
        return False

polling.poll(
    poll_master_alive,
    step=15,
    poll_forever=True
)

master.load()

print("\n" + log_time() + 'SCUDMaster responding')

master_spark_address = 'spark://' + master.private_ip_address + ':7077'
 
print(log_time() + 'Attaching workers to ' + master_spark_address)

# Now launch the workers, given the master (internal) IP

slave_script = """#cloud-boothook
#!/bin/bash
exec > >(tee /var/log/user-data.log|logger -t user-data -s 2>/dev/console) 2>&1
echo export AWS_ACCESS_KEY_ID=""" + os.environ['AWS_ACCESS_KEY_ID'] + """ >> """ + get_spark_path() + """/sbin/spark-config.sh
echo export AWS_SECRET_ACCESS_KEY=""" + os.environ['AWS_SECRET_ACCESS_KEY'] + """ >> """ + get_spark_path() + """/sbin/spark-config.sh
""" + memory + \
(("\nsudo -H -u ubuntu " + get_spark_path() + "/bin/spark-class org.apache.spark.deploy.worker.Worker " + master_spark_address + ' -m ' + executor_memory + ' &') * args.executor_count)

slaves = ec2.create_instances(ImageId=aws.SCUD_AMI, InstanceType=args.worker_type,
                              KeyName=key_name,
                              SubnetId=aws.SCUD_SUBNET,
                              SecurityGroupIds=[aws.SCUD_SG],
                              UserData=slave_script,
                              MinCount=args.size, MaxCount=args.size)

time.sleep(5)

for counter, slave in enumerate(slaves):
    slave.wait_until_running()
    ec2.create_tags(Resources=[slave.id], Tags=[{'Key': 'Name', 'Value': (basename % 'SCUDSlave_') + '_'  + str(counter)}])
    print(log_time() + 'SCUDSlave' + str(counter) + ' = ' + slave.private_ip_address)


# Now wait for master to acknowledge (and name) its workers

sys.stdout.write(log_time() + 'Polling SCUDMaster to acknowldge its workers')
sys.stdout.flush()

worker_names = {}

def poll_master_populated():
    re_worker = re.compile('\s*<a href="http://(\d+\.\d+.\d+.\d+):8081">(worker-[^<]+)</a>')
    try:
        worker_names = {}
        for line in requests.get('http://' + master.private_ip_address + ':8080').iter_lines():
            match = re_worker.match(line)
            if match:
                worker_names[match.group(1)] = match.group(2)
        if len(worker_names) < args.size:
            sys.stdout.write('.')
            sys.stdout.flush()
            return False
        else:
            return True 
    except:
        sys.stdout.write('.')
        sys.stdout.flush()
        return False

polling.poll(
    poll_master_populated,
    step=15,
    poll_forever=True
)

print("\n" + log_time() + 'Master acknowledges workers:')
for worker in worker_names:
    print(worker + ': ' + worker_names[worker])

print(log_time() + 'Transferring jar')

call(['scp',
      '-i', args.key,
       '-o', 'StrictHostKeyChecking=no', 
      args.jar,
      "ubuntu@" + master.private_ip_address + ':'])

if args.conf:
    print(log_time() + 'Transferring conf')
    call(['scp',
          '-i', args.key,
          '-o', 'StrictHostKeyChecking=no', 
          args.conf,
          'ubuntu@{}:{}'.format(master.private_ip_address, get_spark_path())])

if args.dir:
    # Check if the path is a valid dir
    if not os.path.isdir(args.dir):
        sys.stderr.write("'{0}' is not a dir\n".format(args.dir))
        sys.exit(2)

    # It's a dir! Congratulations. Copy it to the driver.
    call(['scp', '-r',
          '-i', args.key,
          '-o', 'StrictHostKeyChecking=no', 
          args.dir,
          "ubuntu@" + master.private_ip_address + ':~/'])

print(log_time() + 'Starting job')

submit_command = 'export AWS_ACCESS_KEY_ID=' + os.environ['AWS_ACCESS_KEY_ID'] + '; ' + \
                 'export AWS_SECRET_ACCESS_KEY=' + os.environ['AWS_SECRET_ACCESS_KEY'] + '; ' + \
                 get_spark_path() + '/bin/spark-submit ' + \
                 '--master ' + master_spark_address + \
                 ' --class ' + args.main + \
                 ' --conf spark.executor.memory=' + executor_memory + ' ' \
                 '/home/ubuntu/' + os.path.split(args.jar)[1] + ' ' + \
                 args.input + ' ' + \
                 args.output + \
                 ' > driver.out 2> driver.err'

call(['ssh',
      '-i', args.key,
       '-o', 'StrictHostKeyChecking=no', 
      "ubuntu@" + master.private_ip_address,
      submit_command])

print(log_time() + 'Finished job')

basename = basename % '/var/log/scud/'

print(log_time() + 'Will copy logs to %s' % basename)

call(['scp',
      '-i', args.key,
       '-o', 'StrictHostKeyChecking=no', 
      "ubuntu@" + master.private_ip_address + ':driver.out',
      basename + '.out'])

call(['scp',
      '-i', args.key,
       '-o', 'StrictHostKeyChecking=no', 
      "ubuntu@" + master.private_ip_address + ':driver.err',
      basename + '.err'])

call(['scp',
      '-i', args.key,
       '-o', 'StrictHostKeyChecking=no', 
      "ubuntu@" + master.private_ip_address + ':driver.log',
      basename + '.log'])

# Shut it down, shut it all down!

print(log_time() + 'Terminating')

for slave in slaves:
    slave.terminate()
master.terminate()

print(log_time() + 'Done')
