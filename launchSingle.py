#!/usr/bin/env python

import boto3, aws, argparse, os, sys, base64, datetime, time, re, socket, json, smtplib 
import subprocess as sp

from email.mime.text import MIMEText

if __name__ == '__main__':
    p = argparse.ArgumentParser (description = 'Launches an EC2 instance to harvest key events.')
    p.add_argument('--repo', metavar='repo', type=str, required=True, help='Path to a repo to be scpd and executed on requested instance.')
    p.add_argument('--script', type = str, required = True, help = 'Name of the master script in the repo')
    p.add_argument('--type', type = str, default = 'c3.8xlarge', help = 'EC2 instance type')
    p.add_argument('--dev', type = str, default = 'xvdb', help = 'Name(s) of the local storage devices to be formatted and mounted (quoted if multiple). Repo code will copied to device corresponding to the first element of the list.')
    p.add_argument('--key', type=str, required=True, help='The AWS keypair to use for instances')    
    p.add_argument('--input', metavar='input', type=str, required=True, help='Input(s) for a master script (quoted if multiple)')
    p.add_argument('--env', metavar='env', type=str, required=False, help='environment variable(s) to pass on (quoted if multiple)')
    p.add_argument('--apt', metavar='apt', type=str, required=False, help='apt dependency(s) (quoted if multiple)')
    p.add_argument('--pip', metavar='pip', type=str, required=False, help='pip dependency(s) (quoted if multiple)')
    args = p.parse_args ()

class Mailer:
    """
        Sends an email notification. Arguments: 
            
            To             : a list of recipient email addresses
            LoggerInstance : an instance of class Logger
    """
    def __init__(self, To):
        self.From = 'webuserid-raw-etl@%s' % socket.gethostname() 
        self.To = To 
    
    def send(self, msg, subject):
        msg = MIMEText(msg)
        msg['Subject'] = subject
        msg['From'] = self.From 
        msg['To'] = ', '.join(self.To) 
        s = smtplib.SMTP('localhost')
        s.sendmail(self.From, self.To, msg.as_string())

# Job configuration

conf = {'mailto': 
            ['j.szejda@elsevier.com', 
             'm.hobby@elsevier.com', 
             'b.norton@elsevier.com', 
             'p.wooldridge@elsevier.com'],
        'log_worker': '/var/log/scud/'}

# Make sure directory path is formatted the way we want it to

def dpath(x):
    if x.endswith('/.'):
        return x
    if x.endswith('/'):
        return x + '.'
    else:
        return x + '/.'
    sys.exit('Something went wrong with %s path formatting' % x)

# Parse environment variables

envs = []
if args.env:
    envs = args.env.split(' ')

for env in envs:
    if not env in os.environ:
        sys.stderr.write(env + ' requested but not set in local environment\n')
        sys.exit(2)

# Parse apt dependencies

apts = []
if args.apt:
    apts = args.apt.split(' ')

# Parse pip dependencies

pips = []
if args.pip:
    pips = args.pip.split(' ')

# Parse device names

devs = []
if args.dev:
    devs = args.dev.split(' ')

# Prepare local aws credentials for dispatch

start_time = datetime.datetime.now().strftime("%Y%m%d%H%M%S")

with open('/home/ubuntu/.aws/credentials') as f:
    credentials = ["echo '" + line.strip('\n') + "' >> /home/ubuntu/.aws/credentials" 
                   for line in f.readlines()]

# Prepare block device mappings

block_device_mappings = [] 
for dev in devs:
    block_device_mappings.append({'DeviceName': '/dev/' + dev, 
                                  'Ebs': {'DeleteOnTermination': True, 'VolumeSize': 320}}) 

# Instance setup bash script

user_data = """#cloud-boothook
#!/bin/bash
export DEBIAN_FRONTEND=noninteractive
sudo debconf-set-selections <<< "postfix postfix/mailname string $HOSTNAME"
sudo debconf-set-selections <<< "postfix postfix/main_mailer_type string 'Internet Site'"
sudo apt-get update
""" + \
"".join(map(lambda x: "echo 'export " + x + '="' + os.environ[x] + '"' + "' >> /home/ubuntu/.profile\n", envs)) + \
"".join(map(lambda x: 'apt-get -y install ' + x + "\n", apts)) + \
"".join(map(lambda x: 'pip install ' + x + "\n", pips)) + \
'\n'.join(credentials)

devs_part = \
"""
sudo mkfs.ext4 /dev/%s 
sudo mkdir /home/ubuntu/%s
sudo mount /dev/%s /home/ubuntu/%s
sudo chown -R ubuntu:ubuntu /home/ubuntu/%s
"""

for dev in devs:
    user_data += devs_part % tuple([dev for i in range(5)]) 

user_data += \
""" 
chown ubuntu:ubuntu /home/ubuntu/%s/%s
chmod u+x /home/ubuntu/%s/%s
"""
user_data = user_data % (devs[0], args.script, devs[0], args.script)

def log_time():
    return datetime.datetime.now().strftime("%d/%m/%y %H:%M:%S") + ' INFO: '

ec2 = boto3.resource('ec2')

print log_time() + 'Starting instance...'

# Launch an instance

instance = ec2.create_instances(ImageId = aws.SCUD_AMI, InstanceType = args.type, 
                                KeyName = 'WADS',
                                SubnetId = aws.SCUD_SUBNET,
                                SecurityGroupIds = [aws.SCUD_SG],
                                UserData = user_data,
                                BlockDeviceMappings = block_device_mappings,
                                MinCount = 1, MaxCount = 1)[0]
instance.wait_until_running()
instance.create_tags(Resources=[instance.id], Tags=[{'Key': 'Name', 'Value': 'SCUDSingle_' + os.path.splitext(os.path.basename(args.script))[0] + '_' + start_time}])

# Wait like a puppy:

def wait():
    try:
        res = \
        sp.check_output(['ssh', 
                        '-i', args.key, 
                        '-o', 'StrictHostKeyChecking=no', 
                        'ubuntu@' + instance.private_ip_address, 
                        'echo yay!']).strip('\n')
        if res == 'yay!':
            return False
    except sp.CalledProcessError:
        return True
    return True 

while wait():
    time.sleep(5)
    print log_time() + 'Waiting...' 
print log_time() + 'Instance responding'
    
# Copy repo to the instance

print log_time() + 'Copying repo...'

sp.call(['scp',
        '-r', 
        '-i', args.key,
        '-o', 'StrictHostKeyChecking=no', 
        dpath(args.repo), 
        "ubuntu@" + instance.private_ip_address + ':/home/ubuntu/%s/.' % devs[0]])

# Submit command

print log_time() + 'Launching job...'

submit_command = 'source /home/ubuntu/.profile; python /home/ubuntu/%s/%s %s' % (devs[0], args.script, args.input)

sp.call(['ssh',
        '-i', args.key,
        '-o', 'StrictHostKeyChecking=no', 
        "ubuntu@" + instance.private_ip_address,
        submit_command])

# Fetch the logfile before terminating the instance. 

job_date = time.strftime("%Y-%m-%d-%H-%S", time.gmtime()) 

# Assign remote and local log paths to variables
 
remote_log_path = '/home/ubuntu/script.log'
local_log_path = conf.get('log_worker') + '%s-%s.log' % (args.script.split('.')[0], job_date)

print 'Remote logfile location: %s' % remote_log_path
print 'Local copy location: %s' % local_log_path
 
sp.call(['scp', 
         '-i', args.key, 
         '-o', 'StrictHostKeyChecking=no', 
         'ubuntu@' + instance.private_ip_address + ':%s' % remote_log_path, 
         local_log_path]) 

instance.terminate()

# Check if the job ended with error and email the log if so

if(os.path.isfile(local_log_path)):
    with open(local_log_path, 'r') as f:
        loglines = f.readlines()

        if loglines[-1].find('[ERROR]') != -1:
            m = Mailer(To = conf.get('mailto'))
            m.send(msg = ''.join(loglines), subject = 'ERROR in webuserid-etl-job-%s' % job_date)

            print log_time() + 'Done.'
