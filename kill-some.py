#!/usr/bin/env python3

import boto3, sys, aws

ec2 = boto3.resource('ec2')

descriptions = ec2.meta.client.describe_instances(Filters = [{'Name': 'image-id', 'Values': ['ami-3a1a3050', 'ami-b8aa9bd2', 'ami-b6666adc', aws.SCUD_AMI]}])
instance_ids = {}
instance_statuses = {}

for reservation in descriptions['Reservations']:
    for instance in reservation['Instances']:
        if 'Tags' in instance:
            instance_ids[instance['Tags'][0]['Value']] = instance['InstanceId']
            instance_statuses[instance['Tags'][0]['Value']] = instance['State']
        else:
            instance_ids['Unknown'] = instance['InstanceId']
            instance_statuses['Unknown'] = instance['State']

for i, instance in enumerate(sorted(instance_ids)):
    print(str(i) + ': ' + instance + ' [' + instance_statuses[instance]['Name'] + ']')

print('\nEnter a number to off the geezer.')
print('\t (or any other key to exit)')

selection = input()
try:
    choice = sorted(instance_ids)[int(selection)]
except:
    sys.exit(1)

instances = ec2.instances.filter(InstanceIds=[instance_ids[choice]])
instances.terminate()
