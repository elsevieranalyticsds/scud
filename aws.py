SCUD_AMI='ami-77b54661'
SCUD_SG='sg-6c260714'
SCUD_SUBNET = 'subnet-279c490a'

instance_resources = {
# micro is too small for these RAM heuristics
#    't2.micro': (1.0, 1),
    'm4.large': (8.0, 2),
    'r3.large': (15.25, 2),
    'r3.2xlarge': (61, 8),
    'r3.4xlarge': (122, 16),
# 8xlarge has too many cores for current slave set-up due to Spark bug
#    'r3.8xlarge': (244, 32)
    'c3.4xlarge': (30, 16),
    'c3.2xlarge': (15, 8)
}
