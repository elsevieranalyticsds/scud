#!/bin/bash

# This script creates a gzipped tarball of scud logs older
# than 7 days, uploads it to S3 and cleans /var/log/scud/ up. 

WD=/var/log/scud
LOGFILES=$(find $WD -maxdepth 1 -mtime +7 -and -type f)
TAR_PATH=$WD/scud-logs-$(date +%Y%m%d).tar

if [ "$LOGFILES" = "" ]
then
    exit
fi

touch $WD/_DUMMY
tar --create $WD/_DUMMY > $TAR_PATH
rm $WD/_DUMMY

for LOGFILE in $LOGFILES; do
    tar --append --file $TAR_PATH $LOGFILE; 
done

tar --delete --file $TAR_PATH $WD/_DUMMY
gzip $TAR_PATH

JUST_NAME=$(echo ${TAR_PATH}.gz | sed 's/.*\///g')
aws s3 mv ${TAR_PATH}.gz s3://wads/data/scud/logs/$JUST_NAME

for LOGFILE in $LOGFILES; do
    rm $LOGFILE; 
done
