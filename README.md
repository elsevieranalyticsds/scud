# README #

SCUD allows the automatic launch and termination of EC2-based python scripts and JAR-based SPARK clusters.

## Prerequisites ##

* Access to the SCUD AMI (currently SCUDGanglia = `ami-d11a67c6`) - please request sending your AWS account ID (the SCUD AMI is currently built with Spark 1.6 over Hadoop 2.4 with Ganglia)
* A security group called `SCUD`, with at least the following incoming rules:

|Type|Protocol|Port Range|Source|
|----|--------|----------|------|
|ALL TCP|TCP|0 - 65535|SCUD|
|SSH|TCP|22|[host launching script]|
|Custom|TCP|8080|[host launching script]|

(You may also want to allow incoming traffic, from the host that runs the script or other such, to Ganglia and further Spark web interfaces, e.g. 80 and 4040)

* aws-cli, the AWS Command Line Interface, in a configured state
* Environment variables for AWS credentials to be made available to Spark/Hadoop (propagated throughout cluster and, by convention, passed to the Spark context's Hadoop configuration), e.g.: 
```
$ export AWS_ACCESS_KEY_ID=*****
$ export AWS_SECRET_ACCESS_KEY=****
```
* A key pair with which to access the instances created (it is assumed that the name for your keypair is the base name, less .pem extender, of the file)
* python dependencies that can be installed as follows:
```
$ pip install boto3 polling requests
```
* a fat jar for your job (see https://sdfe13.atlassian.net/wiki/display/ADS/Building+Jars)

## Usage ##

The `launchCluster` script expects the following flags:

|Flag|Usage|Optional|Allowed values|Default|
|----|-----|--------|--------------|-------|
|`--jar`|Path to the JAR file packaging the job||||
|`--main`|The main class within the jar||||
|`--key`|Path to the PEM file for your keypair||||
|`--size`|The number of EC2 instances to use for workers||||
|`--driver_type`|The type of EC2 instance to use for the driver (e.g. m4.large)||||
|`--worker_type`|The type of EC2 instance to use for the workers(e.g. c3.4xlarge)||||
|`--executor_count`|The number of executors per worker|Yes||1|
|`--spark_version`|The Spark vesrion to use|Yes|1.6.0, 2.0.0|1.6.0|
|`--input`|The first positional argument to pass to the submitted job||||
|`--output`|The second positional argument to pass to the submitted job||||
|`--dir`|Path to a directory to be copied to the driver|Yes|||
|`--conf`|A file to copy to the Spark con directory, usually a `log4j.properties` file (a template for which is provided) (Optional)|||||

By convention the job will expect two positional arguments, usually keys for input and output destination, but these can be elided or multiplexed by quoting.

The script will:

* launch first the driver node with a single Spark instance 
* ensure the driver is responsive 
* launch workers nodes, each running one Spark instance per core, connecting automatically to the driver node
* when the workers are shown attached (i.e. cluster ready), scp the JAR to the driver node and submit the job
* capture the stdout and stderr from the driver
* copy the stdout, stderr and log (if present as driver.log, as per log4j.properties provided) back to the launching host
* terminate the cluster.

## Exceptional Behaviours ##

In case of non-termination, more logging is available on the instances in the cluster at:

* `~ubuntu/spark[]/logs` 
*  `/var/log/user-data.log`

The instances can be removed with the 'kill switch', `kill-all.sh`, but please note that this will indiscrimately terminate all instances of the SCUD AMI accessible by the AWS CLI user.

## Example ##
