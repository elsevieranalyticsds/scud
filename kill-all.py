#!/usr/bin/env python

import boto3, aws

ec2 = boto3.resource('ec2')

instances = ec2.instances.filter(
    Filters = [{'Name': 'image-id', 'Values': ['ami-3a1a3050', 'ami-b8aa9bd2', 'ami-b6666adc', aws.SCUD_AMI]}])
instances.terminate()
